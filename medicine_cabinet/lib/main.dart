import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:medicine_cabinet/models/user.dart';
import 'package:medicine_cabinet/screens/home/gmaps/geolocator_service.dart';
import 'package:medicine_cabinet/screens/home/gmaps/place.dart';
import 'package:medicine_cabinet/screens/home/gmaps/places_service.dart';
import 'package:medicine_cabinet/screens/wrapper.dart';
import 'package:medicine_cabinet/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:permission/permission.dart';
import 'package:google_api_availability/google_api_availability.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final locationService = GeoLocatorService();
  final placesService = PlacesService();
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          FutureProvider(create: (context) => locationService.getLocation()),
          FutureProvider(create: (context) {
            ImageConfiguration configuration =
            createLocalImageConfiguration(context);
            return BitmapDescriptor.fromAssetImage(
                configuration, 'assets/images/icon-pharmacy.png');
          }),
          ProxyProvider2<Position, BitmapDescriptor, Future<List<Place>>>(
            update: (context, position, icon, places) {
              return (position != null)
                  ? placesService.getPlaces(
                  position.latitude, position.longitude, icon)
                  : null;
            },
          )
        ],
      child: StreamProvider<User>.value(
        value:AuthService().user,
        child: MaterialApp(
          home: Wrapper(),
        ),
      ),
    );
  }
}
