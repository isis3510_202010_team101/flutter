import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

import 'package:medicine_cabinet/models/medicine.dart';
import 'package:medicine_cabinet/models/shopMedicine.dart';
import 'package:medicine_cabinet/services/local_database.dart';


class DatabaseService{

  final CollectionReference cabinetCollection=Firestore.instance.collection("userMedicines");
  final CollectionReference shopCollection=Firestore.instance.collection("globalMedicines");
  final String uid;

  DatabaseService({this.uid});

  Future updateUserData(Medicine medicine, String name) async{
    return await cabinetCollection.document(uid).setData({
      'name': name,
      'medicines':[],
    });
  }

  Future addMedicine(Medicine medicine) async{
    // print(medicine.dose);
    return await cabinetCollection.document(uid).updateData({"medicines": FieldValue.arrayUnion([{'name':medicine.name,'image':medicine.image,'dose':medicine.dose,'desc':medicine.desc,'expDate':medicine.expDate,'adverseUses':medicine.adverseUses}])});
  }

  Future<List<ShopMedicine>> getShopMedicine(String filter) async{
    QuerySnapshot eventsQuery = await shopCollection
        .getDocuments();
    print("filter $filter");
    List<ShopMedicine> list =(eventsQuery.documents[0]["medicineList"].map<ShopMedicine>((doc){

      return ShopMedicine(
        name:doc["name"]??"",
        image:doc["image"]??"",
        desc: doc["desc"]??"",
        adverseUses:doc["adverseUses"]??"",
        dose: doc["dose"]??"",
      );
    }).toList());
    list.removeWhere((element) => !element.name.toLowerCase().contains(filter.toLowerCase()));
    return list;
  }
  Future<bool> checkConnect()async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult== ConnectivityResult.none? false:true;
  }

  Future<List<Medicine>> _medicineListFromLocal()async{
    return LocalDatabase.db.getCabinet();
  }
  List<Medicine> _medicineListFromSnapshot(QuerySnapshot snapshot){
    // print("uid: $uid");
    return snapshot.documents.firstWhere((elem) => elem.documentID==uid)["medicines"].map<Medicine>((doc){
       // print(doc);

      Medicine med= Medicine(
        name:doc["name"]??"",
        expDate:doc["expDate"]??"",
        image:doc["image"]??"",
        desc: doc["desc"]??"",
        adverseUses:doc["adverseUses"]??"",
        dose: doc["dose"]??"",
      );

        LocalDatabase.db.insert(med);

      return med;
    }).toList();
  }

  List<ShopMedicine> _shopListFromSnapshot(QuerySnapshot snapshot){
    return snapshot.documents[0]["medicineList"].map<ShopMedicine>((doc){
      return ShopMedicine(
        name:doc["name"]??"",
        image:doc["image"]??"",
        desc: doc["desc"]??"",
        adverseUses:doc["adverseUses"]??"",
        dose: doc["dose"]??"",
      );
    }).toList();
  }
  Stream<List<Medicine>> get medicinesLocal async* {

     yield await _medicineListFromLocal();

  }
  Stream<List<Medicine>> get medicines{

      return cabinetCollection.snapshots()
          .map(_medicineListFromSnapshot);



  }
  Stream<List<ShopMedicine>> get shop{
    return shopCollection.snapshots()
        .map(_shopListFromSnapshot);

  }
}