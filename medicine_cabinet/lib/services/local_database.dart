import 'dart:async';

import 'package:medicine_cabinet/models/medicine.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class LocalDatabase {
  static const String TABLE_CABINET = "cabinet";
  static const String COLUMN_NAME = "name";
  static const String COLUMN_EXPDATE = "expDate";
  static const String COLUMN_IMAGE = "image";
  static const String COLUMN_DESC = "desc";
  static const String COLUMN_DOSE = "dose";
  static const String COLUMN_ADVERSEUSES = "adverseUses";

  LocalDatabase._();

  static final LocalDatabase db = LocalDatabase._();
  Database _database;
  final _medicinesController = StreamController<List<Medicine>>.broadcast();
  StreamSink<List<Medicine>> get _inMedicines => _medicinesController.sink;
  Stream<List<Medicine>> get medicinesLocal => _medicinesController.stream;



  void dispose() {
    _medicinesController.close();
  }
  void getMedicines() async {
    // Retrieve all the notes from the database
    List<Medicine> meds = await getCabinet();

    // Add all of the notes to the stream so we can grab them later from our pages
    _inMedicines.add(meds);
  }

  void handleAddMedicine(Medicine med) async {
    // Create the note in the database
    await insert(med);

    // Retrieve all the notes again after one is added.
    // This allows our pages to update properly and display the
    // newly added note.
    getMedicines();
  }

  Future<Database> get database async {
    print("database getter called");
    if (_database != null) {
      return _database;
    }
    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(
        join(dbPath, 'cabinetDB.db'),
        version: 1,
        onCreate: (Database database, int version) async {
          print("Creating cabinet table");
          await database.execute(
            "CREATE TABLE $TABLE_CABINET ("
                "$COLUMN_NAME TEXT PRIMARY KEY,"
                "$COLUMN_EXPDATE TEXT,"
                "$COLUMN_IMAGE TEXT,"
                "$COLUMN_DESC TEXT,"
                "$COLUMN_DOSE TEXT,"
                "$COLUMN_ADVERSEUSES TEXT"
                ")",
          );
        }

    );
  }
  Future <List<Medicine>> getCabinet() async{
    final db=await database;

    var cabinet = await db.query(
      TABLE_CABINET,
      columns: [COLUMN_NAME,COLUMN_EXPDATE,COLUMN_IMAGE,COLUMN_DESC,COLUMN_DOSE,COLUMN_ADVERSEUSES]
    );
    List<Medicine> medicineList =List<Medicine>();
    
    cabinet.forEach((element) {
      Medicine med=Medicine.fromMap(element);

      medicineList.add(med);
    });
    return medicineList;
  }
  Future<Medicine> insert (Medicine med) async{
    final db=await database;
    try {
      int id = await db.insert(TABLE_CABINET, med.toMap());
    }catch(e){

    }
    return med;
  }
}