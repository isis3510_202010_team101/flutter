import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:medicine_cabinet/models/data.dart';

class AnalyticService {
  final CollectionReference analytics=Firestore.instance.collection("analyticsEngine");

  final String uid;
  var actualSession;

 AnalyticService({this.uid});

  Future createRecord(var pActual) async{
    print(pActual);
    return await analytics.document("R3zzkmAHORZDyXIess7h").updateData({"data": FieldValue.arrayUnion([pActual])});

  }

  Future updateRecord(var pInit, var pActual) async{
    var index = 0;
    await getData().then((lista) => {
     index = lista.lastIndexWhere((element) => element.uid==this.uid),
    print(pInit),
    analytics.document("R3zzkmAHORZDyXIess7h").updateData({"data": FieldValue.arrayRemove([pInit])}).then((value) => {
    analytics.document("R3zzkmAHORZDyXIess7h").updateData({"data": FieldValue.arrayUnion([pActual])})
    })
    });
  }

  Future<List<Data>> getData() async{
    QuerySnapshot eventsQuery = await analytics
        .getDocuments();
    List<Data> list =(eventsQuery.documents[0]["data"].map<Data>((doc){
      return Data(
        uid:doc["uid"]??"",
        batteryUsage:doc["batteryUsage"]??"",
        cpuUsage: doc["cpuUsage"]??"",
        cabinetScreenTime:doc["cabinetScreenTime"]??"",
        shopScreenTime: doc["shopScreenTime"]??"",
        mapScreenTime: doc["mapScreenTime"]??"",
        appScreenTime: doc["appScreenTime"]??"",
        os: doc["os"]??"",
        osVersion: doc["osVersion"]??"",
        model: doc["model"]??"",
      );
    }).toList());
    return list;
  }

  Future<double> getCabinetAverageTime() async{
    List<Data> data= await getData();
    double sum;
    data.forEach((element) {sum+=element.cabinetScreenTime;});
    return sum/data.length;
  }

  Future getMostCommonMedicine()async{

  }
}