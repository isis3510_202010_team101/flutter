import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:medicine_cabinet/models/user.dart';
import 'package:medicine_cabinet/services/analyticsEngine.dart';
import 'package:medicine_cabinet/services/database.dart';
class AuthService{

  final FirebaseAuth _auth= FirebaseAuth.instance;

  User  _userFromFirebaseUser(FirebaseUser user){
    return user!=null?User(uid:user.uid):null;
  }
  Stream<User> get user{
    return _auth.onAuthStateChanged.map((FirebaseUser user) => _userFromFirebaseUser(user));
  }

  //anon sign in
  Future signInAnon() async{
    try{
      AuthResult result= await _auth.signInAnonymously();
      FirebaseUser user= result.user;

      return _userFromFirebaseUser(user);
    }catch(e){
      print(e.toString());
      return null;
    }
  }
  //email & password sign in
  Future signInWithEmailAndPassword (String email,String password)async{
    try{
        AuthResult result = await _auth.signInWithEmailAndPassword(
            email: email, password: password);
        FirebaseUser user = result.user;
        return _userFromFirebaseUser(user);

    }catch(e){
      print(e.toString());
      return null;
    }
  }
  //email & password register
  Future registerWithEmailAndPassword (String email,String password)async{
    try{
      AuthResult result =await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user =result.user;
      await DatabaseService(uid:user.uid).updateUserData(null, 'New User');
      return _userFromFirebaseUser(user);
    }catch(e){
      print(e.toString());
      return null;
    }
  }
  //sign out
  Future signOut() async{
    try{
      return await _auth.signOut();
    }catch(e){
      print(e.toString());
      return null;
    }
  }

}

