class Data{
  final String uid;
  final int batteryUsage;
  final int cpuUsage;
  final int cabinetScreenTime;
  final int shopScreenTime;
  final int mapScreenTime;
  final int appScreenTime;
  final String os;
  final String osVersion;
  final String model;
  Data({this.uid,this.batteryUsage,this.cpuUsage,this.cabinetScreenTime,this.shopScreenTime,this.mapScreenTime,this.appScreenTime,this.os,this.osVersion,this.model});
}