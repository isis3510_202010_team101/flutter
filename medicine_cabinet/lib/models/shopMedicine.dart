class ShopMedicine{
  final String name;
  final String image;
  final String desc;
  final String dose;
  final String adverseUses;
  ShopMedicine({this.name,this.image,this.desc,this.dose,this.adverseUses});
}