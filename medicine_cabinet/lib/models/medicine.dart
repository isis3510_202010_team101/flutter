import 'package:medicine_cabinet/services/local_database.dart';

class Medicine{
   String name;
   String expDate;
   String image;
   String desc;
   String dose;
   String adverseUses;
  Medicine({this.name,this.expDate,this.image,this.desc,this.dose,this.adverseUses});

  Map<String,dynamic> toMap(){
   var map=<String,dynamic>{
     LocalDatabase.COLUMN_NAME:name,
     LocalDatabase.COLUMN_EXPDATE:expDate,
     LocalDatabase.COLUMN_IMAGE:image,
     LocalDatabase.COLUMN_DESC:desc,
     LocalDatabase.COLUMN_DOSE:dose,
     LocalDatabase.COLUMN_ADVERSEUSES:adverseUses,

   };
   return map;
  }

  Medicine.fromMap(Map<String,dynamic> map){
    name= map[LocalDatabase.COLUMN_NAME];
    expDate= map[LocalDatabase.COLUMN_EXPDATE];
    image= map[LocalDatabase.COLUMN_IMAGE];
    desc= map[LocalDatabase.COLUMN_DESC];
    dose= map[LocalDatabase.COLUMN_DOSE];
    adverseUses= map[LocalDatabase.COLUMN_ADVERSEUSES];
  }
}