import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:medicine_cabinet/services/auth.dart';
import 'package:medicine_cabinet/services/local_database.dart';
import 'package:medicine_cabinet/shared/constants.dart';
import 'package:medicine_cabinet/shared/loading.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;
  SignIn({this.toggleView});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth=AuthService();
  final _formKey =GlobalKey<FormState>();
  bool loading=false;
  String email="";
  String password="";
  String error="";
  @override
  Widget build(BuildContext context) {
    return loading? Loading():Scaffold(
      backgroundColor: Colors.amber[50],
      appBar: AppBar(
        backgroundColor: Colors.indigo[800],
        elevation: 0,
        title: Text("Sign in"),
        actions: [
          FlatButton.icon(
              onPressed: (){
                widget.toggleView();
              },
              icon: Icon(Icons.person),
              label: Text("Register"))
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(height:20),
              TextFormField(
                decoration: textInputDecoration.copyWith(hintText: 'Email'),
                validator: (val){
                  return val.isEmpty? "Enter an email":null;
                },
                onChanged: (val){
                  setState(() {
                    email=val;
                  });
                },
              ),
              SizedBox(height:20),
              TextFormField(
                decoration: textInputDecoration.copyWith(hintText: 'Password'),
                validator: (val){
                  return val.length<6? "Enter a password with 6 or more characters":null;
                },
                obscureText: true,
                onChanged: (val){
                  setState(() {
                    password=val;
                  });
                },
              ),
              SizedBox(height:20),
              RaisedButton(
                color: Colors.indigo[800],
                child: Text(
                  "Sign in",
                  style: TextStyle(
                    color:Colors.white,
                  ),
                ),
                onPressed: ()async{
                  var connectivityResult = await (Connectivity().checkConnectivity());
                  if(connectivityResult!=ConnectivityResult.none) {
                    if (_formKey.currentState.validate()) {
                      setState(() {
                        loading = true;
                      });
                      dynamic result = await _auth.signInWithEmailAndPassword(
                          email, password);
                      if (result == null) {
                        setState(() {
                          error =
                          "Please supply a valid email and password combination";
                          loading = false;
                        });
                      }
                    }
                  }else{
                    setState(() {
                      error =
                      "Couldn't log in. No internet connection was found.";
                    });
                  }
                },
              ),
              SizedBox(height: 20,),
              Text(
                error,
                style:TextStyle(
                  color:Colors.red,
                  fontSize: 14,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
