import 'package:flutter/material.dart';
import 'package:medicine_cabinet/models/user.dart';
import 'package:medicine_cabinet/screens/authenticate/authenticate.dart';
import 'package:medicine_cabinet/screens/home/home.dart';
import 'package:medicine_cabinet/screens/home/shop.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user=Provider.of<User>(context);

    return user==null?Authenticate():Home();
  }

}
