import 'dart:convert';

import "package:flutter/material.dart";
import 'package:medicine_cabinet/models/user.dart';
import 'package:medicine_cabinet/screens/home/shop.dart';
import 'package:medicine_cabinet/screens/home/cabinet.dart';
import 'package:medicine_cabinet/screens/home/map.dart';
import 'package:connectivity/connectivity.dart';
import 'package:provider/provider.dart';
import 'package:system_info/system_info.dart';
import 'package:device_info/device_info.dart';
import 'package:medicine_cabinet/services/analyticsEngine.dart';
import 'dart:io' show Platform;

class Screens extends StatefulWidget {
  @override
  _ScreensState createState() => _ScreensState();
}

class _ScreensState extends State<Screens> with WidgetsBindingObserver {
  Stopwatch stopwatch = new Stopwatch()..start();
  int durationCabinet = 0;
  int durationCatalog = 0;
  int durationMap = 0;
  bool mapCharged = false;
  bool resume = false;
  String scr = "Cabinet";
  var initSession;
  var actualSession;


  void toggleView(String screen) {
    setState(() {
      scr = screen;
    });
  }

  String userId = "";
  AppLifecycleState _lastLifecycleState;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    stopwatch.stop();
    setState(() {
      _lastLifecycleState = state;
    });
    if (state == AppLifecycleState.paused) {
      if (scr == "cabinet") {
        durationCabinet += stopwatch.elapsedMilliseconds;
      }
      if (scr == "shop") {
        durationCatalog += stopwatch.elapsedMilliseconds;
      }
      if (scr == "map") {
        durationMap += stopwatch.elapsedMilliseconds;
      }

      var os = "";
      var osVersion = "";
      var model = "";
      if (Platform.isAndroid) {
        var androidInfo = await DeviceInfoPlugin().androidInfo;
        os = 'Android';
        osVersion = androidInfo.version.release;
        model = androidInfo.manufacturer + " " + androidInfo.model;
      }

      if (Platform.isIOS) {
        var iosInfo = await DeviceInfoPlugin().iosInfo;
        os = iosInfo.systemName;
        osVersion = iosInfo.systemVersion;
        model = iosInfo.name + " " + iosInfo.model;
      }
      int appScreenTime = durationCabinet + durationCatalog + durationMap;
      actualSession={
        'uid':userId,
        'batteryUsage':0,
        'cpuUsage':0,
        'cabinetScreenTime':durationCabinet,
        'shopScreenTime':durationCatalog,
        'mapScreenTime':durationMap,
        'appScreenTime':appScreenTime,
        'os': os,
        'osVersion':osVersion,
        'model':model
      };
      if(resume){
      AnalyticService(uid: userId).updateRecord(initSession, actualSession);
      initSession= jsonDecode(jsonEncode(actualSession));
      }
      else{
      AnalyticService(uid: userId).createRecord(actualSession);
      resume=true;
      initSession= jsonDecode(jsonEncode(actualSession));
      }
      stopwatch.start();
    }
  }

  @override
  Widget build(BuildContext context) {
    try {
      final user = Provider.of<User>(context);
      userId = user.uid;
    } catch (e) {}
    return Stack(
      children: [
        scr == "cabinet"
            ? CabinetList()
            : scr == "shop"
                ? Shop()
                : scr == "map"
                    ? GMap()
                    : CabinetList(),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                color: Colors.black26,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FloatingActionButton(
                      autofocus: true,
                      backgroundColor: scr == "cabinet"
                          ? Colors.blueGrey
                          : Colors.indigo[800],
                      focusColor: Color(0xFFB74093),
                      child: Text('Cabinet', style: TextStyle(fontSize: 12)),
                      onPressed: () {
                        durationCabinet += stopwatch.elapsedMilliseconds;
                        print(durationCabinet);
                        toggleView("cabinet");
                        stopwatch.reset();
                      },
                    ),
                    FloatingActionButton(
                      backgroundColor:
                          scr == "shop" ? Colors.blueGrey : Colors.indigo[800],
                      heroTag: null,
                      child: Text('Catalog', style: TextStyle(fontSize: 12)),
                      onPressed: () {
                        durationCatalog += stopwatch.elapsedMilliseconds;
                        print(durationCatalog);
                        toggleView("shop");
                        stopwatch.reset();
                      },
                    ),
                    FloatingActionButton(
                        backgroundColor: scr == "alarms"
                            ? Colors.blueGrey
                            : Colors.indigo[800],
                        heroTag: null,
                        child: Text('Contact', style: TextStyle(fontSize: 12)),
                        onPressed: () {
                          comingSoon(context);
                        }),
                    FloatingActionButton(
                      backgroundColor:
                          scr == "map" ? Colors.blueGrey : Colors.indigo[800],
                      heroTag: null,
                      child: Text('Map', style: TextStyle(fontSize: 12)),
                      onPressed: () async {
                        var connectivityResult =
                            await (Connectivity().checkConnectivity());
                        if (connectivityResult != ConnectivityResult.none ||
                            mapCharged) {
                          durationMap += stopwatch.elapsedMilliseconds;
                          toggleView("map");
                          stopwatch.reset();
                          mapCharged = true;
                        } else {
                          showAlertDialog(context);
                        }
                      },
                    ),
                  ],
                ))),
      ],
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Connection Failed"),
      content: Text(
          "The pharmacies map could not be charged. Please check your internet connection or try again later."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  comingSoon(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Contact info"),
      content: Text(
          "Julian Amezquita - js.amezquita@uniandes.edu.co - 3142289318"
              "\nCarlos Orduz - ca.orduz@uniandes.edu.co - 3179856234"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
