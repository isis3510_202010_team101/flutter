import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:medicine_cabinet/models/medicine.dart';
import 'package:medicine_cabinet/screens/home/medicine_tile.dart';
import 'package:medicine_cabinet/services/local_database.dart';
import 'package:medicine_cabinet/shared/loading.dart';
import 'package:provider/provider.dart';

class CabinetList extends StatefulWidget {

  @override
  _CabinetListState createState() => _CabinetListState();
}

class _CabinetListState extends State<CabinetList> {

  var cabinet;

  @override
  Widget build(BuildContext context) {
  cabinet=Provider.of<List<Medicine>>(context);



    return Stack(
      children: [
        cabinet==null?Loading():GridView.builder(
          padding: EdgeInsets.fromLTRB(0, 0, 0,  MediaQuery.of(context).size.height * 0.1),
      itemCount:cabinet.length,
          gridDelegate:
          new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
            ),

      itemBuilder: (context,index){
        return MedicineTile(medicine:cabinet[index]);
      },
    ),

      ]);
  }
}

