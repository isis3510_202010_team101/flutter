import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:medicine_cabinet/models/medicine.dart';
import 'package:medicine_cabinet/models/shopMedicine.dart';
import 'package:medicine_cabinet/models/user.dart';
import 'package:medicine_cabinet/screens/home/cabinet.dart';
import 'package:medicine_cabinet/screens/home/gmaps/geolocator_service.dart';
import 'package:medicine_cabinet/screens/home/gmaps/place.dart';
import 'package:medicine_cabinet/screens/home/gmaps/places_service.dart';
import 'package:medicine_cabinet/screens/home/map.dart';
import 'package:medicine_cabinet/screens/home/screens.dart';
import 'package:medicine_cabinet/screens/home/shop.dart';
import 'package:medicine_cabinet/services/auth.dart';
import 'package:medicine_cabinet/services/local_database.dart';
import 'package:provider/provider.dart';
import 'package:medicine_cabinet/services/database.dart';
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _auth = AuthService();
  Map _source = {ConnectivityResult.none: false};
  MyConnectivity _connectivity = MyConnectivity.instance;
  var provider ;
  @override
  void initState() {
    super.initState();
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }
  @override
  Widget build(BuildContext context) {

    final user=Provider.of<User>(context);
    switch (_source.keys.toList()[0]) {
      case ConnectivityResult.none:
       provider =LocalDatabase.db.medicinesLocal;
       print("akiakia");
        break;
      case ConnectivityResult.mobile:
        provider =DatabaseService(uid:user.uid).medicines;
        break;
      case ConnectivityResult.wifi:
        provider =DatabaseService(uid:user.uid).medicines;
    }


      return  StreamProvider<List<ShopMedicine>>.value(
        value: DatabaseService().shop,
        child: StreamProvider<User>.value(
            value: _auth.user,
            child: StreamProvider<List<Medicine>>.value(
              value: provider,
              child: Scaffold(
                resizeToAvoidBottomInset:false,
                backgroundColor: Colors.amber[50],
                appBar: AppBar(
                  title: Text("Medicine Cabinet"),
                  backgroundColor: Colors.indigo[800],
                  elevation:0,
                  actions: [
                    FlatButton.icon(
                      icon:Icon(Icons.person),
                      label:Text("Log Out"),
                      onPressed: () async{
                        await _auth.signOut();
                      },
                    )
                  ],
                ),

                body: Container(

                  child: Stack(
                    children: [
                    Screens(),
              ],
            ),
          ),
              ),
            ),
          ),
      )
    ;
  }
}
class MyConnectivity {
  MyConnectivity._internal();

  static final MyConnectivity _instance = MyConnectivity._internal();

  static MyConnectivity get instance => _instance;

  Connectivity connectivity = Connectivity();

  StreamController controller = StreamController.broadcast();

  Stream get myStream => controller.stream;

  void initialise() async {
    ConnectivityResult result = await connectivity.checkConnectivity();
    _checkStatus(result);
    connectivity.onConnectivityChanged.listen((result) {
      _checkStatus(result);
    });
  }

  void _checkStatus(ConnectivityResult result) async {
    bool isOnline = false;
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        isOnline = true;
      } else
        isOnline = false;
    } on SocketException catch (_) {
      isOnline = false;
    }
    controller.sink.add({result: isOnline});
  }

  void disposeStream() => controller.close();
}
