import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:medicine_cabinet/models/medicine.dart';

class MedicineTile extends StatelessWidget {
  final Medicine medicine;
  MedicineTile({this.medicine});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top:8),
      child: Card(
        elevation: 20,
        color: Colors.grey[300],
        margin: EdgeInsets.fromLTRB(20, 6, 20, 0),
        child: Column(
          children: [
            Flexible(
              child: CachedNetworkImage(
                imageUrl: medicine.image,
                placeholder: (context, url) => CircularProgressIndicator(),

              ),
            ),
            ListTile(
              title: Text(medicine.name, style: TextStyle(fontSize: 15),),
              subtitle: Text(medicine.expDate),
            ),
            FlatButton(
              color: Colors.grey[400],
              onPressed: (){},
              child: Text("Edit"),
            ),
          ],
        ),
      ),
    );
  }
}
