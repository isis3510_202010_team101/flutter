import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:medicine_cabinet/models/medicine.dart';
import 'package:medicine_cabinet/models/shopMedicine.dart';
import 'package:medicine_cabinet/models/user.dart';
import 'package:medicine_cabinet/services/database.dart';
import 'package:medicine_cabinet/services/local_database.dart';
import 'package:provider/provider.dart';
class ShopMedicineTile extends StatefulWidget {

  final ShopMedicine medicine;

  ShopMedicineTile({this.medicine});

  @override
  _ShopMedicineTileState createState() => _ShopMedicineTileState();
}

class _ShopMedicineTileState extends State<ShopMedicineTile> {

  @override
  Widget build(BuildContext context) {
    final user=Provider.of<User>(context);
    return Padding(

      padding: EdgeInsets.only(top:8),
      child: Card(
        elevation: 20,
        color: Colors.grey[300],
        margin: EdgeInsets.fromLTRB(6, 6, 6, 6),
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Column(
          children: [
        Flexible(
          child: CachedNetworkImage(
            imageUrl: widget.medicine.image,
            placeholder: (context, url) => CircularProgressIndicator(),

          ),
        ),
            ListTile(
              title: Text(widget.medicine.name,style: TextStyle(fontSize: 15),),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: FloatingActionButton(
                onPressed: () async{
                  // Add your onPressed code here!
                  var connectivityResult = await (Connectivity().checkConnectivity());
                  if(connectivityResult==ConnectivityResult.none){
                    LocalDatabase.db.handleAddMedicine(new Medicine(name:widget.medicine.name,image: widget.medicine.image,desc: widget.medicine.desc,dose: widget.medicine.dose,adverseUses: widget.medicine.adverseUses,expDate:"Enter expiracy date" ));

                    Connectivity().onConnectivityChanged.listen((result) {if(result==ConnectivityResult.wifi || result == ConnectivityResult.mobile){
                      DatabaseService(uid: user.uid).addMedicine(new Medicine(
                          name: widget.medicine.name,
                          image: widget.medicine.image,
                          desc: widget.medicine.desc,
                          dose: widget.medicine.dose,
                          adverseUses: widget.medicine.adverseUses,
                          expDate: "Enter expiracy date"));
                    }
                    });


                  }else {
                    DatabaseService(uid: user.uid).addMedicine(new Medicine(
                        name: widget.medicine.name,
                        image: widget.medicine.image,
                        desc: widget.medicine.desc,
                        dose: widget.medicine.dose,
                        adverseUses: widget.medicine.adverseUses,
                        expDate: "Enter expiracy date"));
                  }
                    },
                child: Icon(Icons.add),
                backgroundColor: Colors.grey[400],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
