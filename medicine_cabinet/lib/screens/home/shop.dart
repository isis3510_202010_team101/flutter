import 'package:flutter/material.dart';
import 'package:medicine_cabinet/models/shopMedicine.dart';
import 'package:medicine_cabinet/screens/home/shop_medicine_tile.dart';
import 'package:medicine_cabinet/services/database.dart';
import 'package:medicine_cabinet/shared/constants.dart';
import 'package:medicine_cabinet/shared/loading.dart';
import 'package:provider/provider.dart';

class Shop extends StatefulWidget {
  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<Shop> {

  List<ShopMedicine> list;
  @override
  Widget build(BuildContext context) {

    final shop =Provider.of<List<ShopMedicine>>(context);


    return Column(
      children: [
        SafeArea(
          child: Container(
            color: Colors.indigo[800],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width:MediaQuery.of(context).size.width * 0.8,

                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: TextFormField(
                      decoration: textInputDecoration.copyWith(hintText: 'Search'),
                      onChanged: (val){

                        setState(() async{
                          // filter=val;

                          list=await DatabaseService().getShopMedicine(val);
                        });
                      },
                    )
                ),

              ],
            ),
          ),
        ),

        shop==null?Loading():Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.fromLTRB(0, 0, 0,  MediaQuery.of(context).size.height * 0.1),
              itemCount:list==null?shop.length:list.length,
              gridDelegate:
              new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2),
              itemBuilder: (context,index){
                return ShopMedicineTile(medicine:list==null?shop[index]:list[index]);
              },
            ),
          ),
        ),
      ],
    );
  }
}
